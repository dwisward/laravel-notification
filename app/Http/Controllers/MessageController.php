<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\NotificationEven;

class MessageController extends Controller
{
    public function index()
    {
    	return view('index');
    }

    public function store(Request $request)
    {
    	event(new NotificationEven($request->message));
	    return redirect()->route('messages.index')->withSuccess('Berhasil Brodcast');
    }
}
